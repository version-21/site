module.exports = {
  apps: [{
    name: 'Receitas: Test',
    script: './dist/test/server/main.js',
    instances: 1,
    autorestart: true,
    watch: true,
    // max_memory_restart: '250M',
    env: {
      NODE_ENV: 'development',
    },
    env_production: {
      NODE_ENV: 'production',
    },
  }, ],
};

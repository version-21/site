import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentComponent } from './component.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{path: '', component: ComponentComponent}]

@NgModule({
  declarations: [ComponentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ComponentModule { }
